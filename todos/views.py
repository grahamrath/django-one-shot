from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


def show_todolist(request):
    todolist = TodoList.objects.all()
    context = {
        "alltodolists": todolist,
    }
    return render(request, "todos/list.html", context)


def details_todolist(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    context = {
        "todolist": todolist,
    }
    return render(request, "todos/detail.html", context)


def create_todolist(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
        }
    return render(request, "todos/createlist.html", context)


def edit_todolist(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm(instance=todolist)
    context = {
        "todolist": todolist,
        "todolistform": form,
    }
    return render(request, "todos/editlist.html", context)


def delete_todolist(request, id):
    todolist = TodoList.objects.get(id=id)
    if request.method == "POST":
        todolist.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def create_todoitem(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
        }
    return render(request, "todos/createitem.html", context)


def edit_todoitem(request, id):
    todoitem = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todoitem)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm(instance=todoitem)
    context = {
        "todoitem": todoitem,
        "todoitemform": form,
    }
    return render(request, "todos/edititem.html", context)
